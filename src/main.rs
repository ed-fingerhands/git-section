#![feature(try_blocks)]

use crate::args::{BranchArg, StatusArg};
use args::Options;
use git2::{Branch, BranchType, Reference, Repository, Status, StatusEntry, StatusOptions, ErrorCode};
use snafu::{OptionExt, ResultExt, Snafu};
use std::fmt::Write;
use std::iter::FromIterator;
use std::{env, io, str};

mod args;

#[derive(Debug, Snafu)]
enum Error {
    #[snafu(display("Unable to access current working dir: {}", source))]
    GetCurrentDir { source: io::Error },

    #[snafu(display("Unable to access current git repository: {}", source))]
    GetRepository { source: git2::Error },

    #[snafu(display("Unable to get repository branches: {}", source))]
    GetBranches { source: git2::Error },

    #[snafu(display("Unable to get current branch"))]
    GetCurrentBranch,

    #[snafu(display("Unable to get head reference: {}", source))]
    GetHead { source: git2::Error },

    #[snafu(display("Unable to get reference target"))]
    GetTarget,

    #[snafu(display(
        "Unable to get commit count difference between local and remote: {}",
        source
    ))]
    GetAheadBehind { source: git2::Error },

    #[snafu(display("Unable to get branch name: {}", source))]
    GetBranchName { source: git2::Error },

    #[snafu(display("Branch name '{}' is not valid utf8: {}", name, source))]
    BranchNameUtf8 {
        name: String,
        source: str::Utf8Error,
    },

    #[snafu(display("Unable to get file statuses: {}", source))]
    GetFileStatuses { source: git2::Error },
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[paw::main]
fn main(args: Options) {
    let output: Result<String> = try {
        let mut output = Vec::new();
        let repo: Repository = {
            let cwd = env::current_dir().context(GetCurrentDir)?;
            Repository::discover(cwd).context(GetRepository)?
        };

        current_branch(&repo, &mut output, args.branch)?;
        commit_count_diff(&repo, &mut output, args.commits)?;
        files_status(&repo, &mut output, args.status)?;

        output.join(" ")
    };

    match output {
        Err(Error::GetRepository { ref source }) if source.code() == ErrorCode::NotFound => {},
        Err(err) => eprintln!("{:#}", err),
        Ok(out) => println!("{}", out),
    }
}

fn current_branch(repo: &Repository, output: &mut Vec<String>, arg: BranchArg) -> Result<()> {
    if arg == BranchArg::None {
        return Ok(());
    }

    let branch: Branch = repo
        .branches(Some(BranchType::Local))
        .context(GetBranches)?
        .flatten()
        .map(|b| b.0)
        .find(Branch::is_head)
        .context(GetCurrentBranch)?;

    let name = branch.name_bytes().context(GetBranchName)?;
    let name = str::from_utf8(name).context(BranchNameUtf8 {
        name: String::from_utf8_lossy(name),
    })?;

    let mut out = format!(" {}", name);

    if arg == BranchArg::Full {
        if let Ok(remote) = branch.upstream() {
            let remote = remote.name_bytes().context(GetBranchName)?;
            let remote = str::from_utf8(remote).context(BranchNameUtf8 {
                name: String::from_utf8_lossy(remote),
            })?;

            out.write_fmt(format_args!(":{}", remote)).unwrap();
        }
    };

    output.push(out);

    Ok(())
}

fn commit_count_diff(repo: &Repository, output: &mut Vec<String>, status: StatusArg) -> Result<()> {
    if status == StatusArg::None {
        return Ok(());
    }

    let head: Reference = repo.head().context(GetHead)?;
    let head_target = head.target().context(GetTarget)?;

    let remote: Result<Branch, _> = Branch::wrap(head).upstream();

    if let Ok(remote) = remote {
        let (ahead, behind) = repo
            .graph_ahead_behind(head_target, remote.get().target().context(GetTarget)?)
            .context(GetAheadBehind)?;

        if status == StatusArg::Icon {
            if ahead != 0 {
                output.push(String::from("⇡"));
            }
            if behind != 0 {
                output.push(String::from("⇣"));
            }
        } else if status == StatusArg::Full {
            if ahead != 0 {
                output.push(format!("{}⇡", ahead));
            }
            if behind != 0 {
                output.push(format!("{}⇣", behind));
            }
        }
    }

    Ok(())
}

#[derive(Default)]
struct Stats {
    conflicted: usize,
    staged: usize,
    unstaged: usize,
    untracked: usize,
}

impl<'r> FromIterator<StatusEntry<'r>> for Stats {
    fn from_iter<T: IntoIterator<Item = StatusEntry<'r>>>(iter: T) -> Self {
        let (staged, unstaged, untracked) = flags();

        let mut full = Self::default();

        iter.into_iter().for_each(|s: StatusEntry| {
            if s.status().is_conflicted() {
                full.conflicted += 1
            }
            if s.status().intersects(staged) {
                full.staged += 1;
            }
            if s.status().intersects(unstaged) {
                full.unstaged += 1;
            }
            if s.status().intersects(untracked) {
                full.untracked += 1;
            }
        });

        full
    }
}

fn files_status(repo: &Repository, output: &mut Vec<String>, status: StatusArg) -> Result<()> {
    if status == StatusArg::None {
        return Ok(());
    }

    let mut stat_opts = StatusOptions::new();

    let stats: Stats = repo
        .statuses(Some(stat_opts.include_untracked(true)))
        .context(GetFileStatuses)?
        .iter()
        .collect();

    if status == StatusArg::Icon {
        let c = if stats.conflicted > 0 { "~" } else { "" };
        let s = if stats.staged > 0 { "+" } else { "" };
        let u = if stats.unstaged > 0 { "!" } else { "" };
        let t = if stats.untracked > 0 { "?" } else { "" };

        output.push(format!("[{}{}{}{}]", c, s, u, t))
    } else if status == StatusArg::Full {
        let mut out = String::from("[");

        if stats.conflicted > 0 {
            out.write_fmt(format_args!("{}~", stats.conflicted))
                .unwrap();
        };

        if stats.staged > 0 {
            if out.len() > 1 {
                out.push_str(", ")
            }
            out.write_fmt(format_args!("{}+", stats.staged)).unwrap();
        };
        if stats.unstaged > 0 {
            if out.len() > 1 {
                out.push_str(", ")
            }
            out.write_fmt(format_args!("{}!", stats.unstaged)).unwrap();
        };
        if stats.untracked > 0 {
            if out.len() > 1 {
                out.push_str(", ")
            }
            out.write_fmt(format_args!("{}?", stats.untracked)).unwrap();
        };
        out.push(']');

        output.push(out)
    }
    Ok(())
}

#[inline(always)]
fn flags() -> (Status, Status, Status) {
    let staged = Status::INDEX_NEW
        | Status::INDEX_MODIFIED
        | Status::INDEX_DELETED
        | Status::INDEX_RENAMED
        | Status::INDEX_TYPECHANGE;

    let unstaged =
        Status::WT_MODIFIED | Status::WT_DELETED | Status::WT_RENAMED | Status::WT_TYPECHANGE;

    let untracked = Status::WT_NEW;

    (staged, unstaged, untracked)
}

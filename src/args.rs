use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Options {
    /// Show branch names
    #[structopt(short, long, parse(from_occurrences = "From::from"))]
    pub branch: BranchArg,

    /// Show commits ahead and behind
    #[structopt(short, long, parse(from_occurrences = "From::from"))]
    pub commits: StatusArg,

    /// Show file status
    #[structopt(short, long, parse(from_occurrences = "From::from"))]
    pub status: StatusArg,
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum BranchArg {
    None = 0,
    Local = 1,
    Full = 2,
}

impl From<u64> for BranchArg {
    fn from(v: u64) -> Self {
        match v {
            0 => BranchArg::None,
            1 => BranchArg::Local,
            _ => BranchArg::Full,
        }
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum StatusArg {
    None = 0,
    Icon = 1,
    Full = 2,
}

impl From<u64> for StatusArg {
    fn from(v: u64) -> Self {
        match v {
            0 => StatusArg::None,
            1 => StatusArg::Icon,
            _ => StatusArg::Full,
        }
    }
}
